﻿using NodeData.Models;
using System.Collections.Generic;

namespace Web_Node.Models
{
    public class TaskListViewModel
    {
        public IList<TaskList> Tasks { get; set; }
        public TaskList Task { get; set; }
        public Profile Profile { get; set; }

    }
}