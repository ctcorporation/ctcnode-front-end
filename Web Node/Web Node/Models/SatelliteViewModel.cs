﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web_Node.Models
{
    public class SatelliteViewModel
    {
    }
    public class HeartBeatList
    {
        [Display(Name = "Customer Name")]
        public string Customer { get; set; }
        [Display(Name = "Last Operation")]
        public string LastOperation { get; set; }
        [Display(Name = "Application/Satellite Name")]
        public string ApplicationName { get; set; }
        [Display(Name = "Opened")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:MM/dd/yyyy hh:mm ttt}")]
        public DateTime? Opened { get; set; }
        [Display(Name = "Last Check in")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:MM/dd/yyyy hh:mm ttt}")]
        public DateTime? LastCheckin { get; set; }
        [Display(Name = "Is Running")]
        public bool IsRunning { get; set; }
        [Display(Name = "Monitored App")]
        public bool IsMonitored { get; set; }

    }
}
