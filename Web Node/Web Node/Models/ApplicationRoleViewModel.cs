﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace Web_Node.Models
{
    public class ApplicationRoleViewModel
    {
        public IdentityRole Role { get; set; }
        public IEnumerable<ApplicationUser> Users { get; set; }

    }
}
