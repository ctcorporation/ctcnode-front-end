﻿using NodeData.Models;
using System.Collections.Generic;

namespace Web_Node.Models
{
    public class CustomerViewModel
    {
        public IList<Customer> Customers { get; set; }

        public Customer Customer { get; set; }

        public string EditMode { get; set; }
    }
}