﻿using Microsoft.Owin;
using NodeData.Models;
using Owin;
using System.Data.Entity;

[assembly: OwinStartupAttribute(typeof(Web_Node.Startup))]
namespace Web_Node
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            DbConfiguration.SetConfiguration(new NodeDBConfiguration());
            ConfigureAuth(app);
        }
    }
}
