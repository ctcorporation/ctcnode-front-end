﻿using Microsoft.AspNet.Identity.EntityFramework;
using NodeData.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web_Node.Models
{
    public class ApplicationUserViewModel
    {
        public ApplicationUser User { get; set; }

        public IEnumerable<IdentityRole> Roles { get; set; }

        public Customer Customer { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }




}