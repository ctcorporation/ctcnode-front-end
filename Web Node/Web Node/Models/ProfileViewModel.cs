﻿using NodeData.Models;
using System.Collections.Generic;

namespace Web_Node.Models
{
    public class ProfileViewModel
    {
        public IList<vw_CustomerProfile> Profiles { get; set; }

        public vw_CustomerProfile CustProfile { get; set; }

        public Profile Profile { get; set; }

        public string EditMode { get; set; }
    }
}