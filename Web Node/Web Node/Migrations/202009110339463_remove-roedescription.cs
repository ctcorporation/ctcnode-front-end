﻿namespace Web_Node.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeroedescription : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetRoles", "Description");
            DropColumn("dbo.AspNetRoles", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.AspNetRoles", "Description", c => c.String());
        }
    }
}
