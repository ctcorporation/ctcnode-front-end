﻿using NodeData.Models;
using System;
using System.Collections.Generic;

namespace Web_Node.Models
{
    public class CustomerList
    {
        public string CustomerName { get; set; }
        public Guid Id { get; set; }
    }

    public class ProfileList
    {
        public string Profiledescription { get; set; }
        public Guid Id { get; set; }
    }

    public class TransactionsViewModel
    {
        public IEnumerable<CustomerList> Customers { get; set; }
        public IEnumerable<ProfileList> CustomersProfiles { get; set; }
        public IEnumerable<TaskList> Tasklists { get; set; }
        public IEnumerable<ApiLog> ApiLogs { get; set; }




    }
}