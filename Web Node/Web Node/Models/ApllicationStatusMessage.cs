﻿namespace Web_Node.Models
{
    public class ApplicationStatusMessage
    {
        public string Message { get; set; }
        public string Operation { get; set; }
        public bool Success { get; set; }
    }
}