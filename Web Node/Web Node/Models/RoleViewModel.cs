﻿using System.ComponentModel.DataAnnotations;

namespace Web_Node.Models
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        [Required]
        [Display(Name = "Role Name")]
        public string Name { get; set; }

    }
}