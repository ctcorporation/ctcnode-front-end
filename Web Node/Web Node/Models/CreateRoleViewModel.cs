﻿using System.ComponentModel.DataAnnotations;

namespace Web_Node.Models
{
    public class CreateRoleViewModel
    {
        [Required]
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
}