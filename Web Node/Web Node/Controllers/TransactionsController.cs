﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Web_Node.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Web;

namespace Web_Node.Controllers
{

    [System.Web.Mvc.Authorize]
    public class TransactionsController : Controller
    {
        // GET: Transactions
        public async Task<ActionResult> Index()
        {

            var customerList = await GetCustomersAsync();

            
            ViewBag.CustomerList = new SelectList(customerList, "CustomerName", "Id");

            return View();
        }

        public async Task<List<CustomerList>> GetCustomersAsync()
        {

            ApplicationUserManager UserManager;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                if (User.IsInRole("Administrator"))
                {
                    var customers = uow.Customers.Find(x => x.C_IS_ACTIVE == "Y")
                                .Select(y => new CustomerList
                                {
                                    CustomerName = y.C_NAME,
                                    Id = y.C_ID
                                })
                                .OrderBy(c => c.CustomerName);
                    return await customers.ToListAsync();
                }
                else
                {
                    UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
                    var customers = uow.Customers.Find(x => x.C_IS_ACTIVE == "Y" && x.C_CODE == user.CompanyCode)
                                .Select(y => new CustomerList
                                {
                                    CustomerName = y.C_NAME,
                                    Id = y.C_ID
                                })
                                .OrderBy(c => c.CustomerName);
                    return await customers.ToListAsync();
                }
                

                

            }


        }

        private async Task<JsonResult> GetProfileListAsync(Guid custid)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                var profileList = await uow.Profiles.Find(x => x.P_C == custid)
                                        .Select(y => new ProfileList
                                        {
                                            Profiledescription = y.P_DESCRIPTION,
                                            Id = y.P_ID
                                        })
                                   .OrderBy(x => x.Profiledescription).ToListAsync();
                var li = new List<SelectListItem>();
                foreach (var p in profileList)
                {
                    li.Add(new SelectListItem { Text = p.Profiledescription, Value = p.Id.ToString() });
                }

                return Json(li, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FindTransactions(string CustID, FormCollection formValues)
        {


            return RedirectToAction("Index");
        }



    }
}