﻿using NodeData;
using NodeData.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Web_Node.Models;

namespace Web_Node.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<HeartBeatList> hbList = new List<HeartBeatList>();
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext()))
            {
                if (User.IsInRole("Administrator"))
                {
                    hbList = (from hb in uow.HeartBeats.GetAll()
                                    .Include(x => x.CustomerLookup)
                              select new HeartBeatList
                              {
                                  ApplicationName = hb.HB_NAME,
                                  Customer = hb.CustomerLookup.C_NAME,
                                  IsMonitored = hb.HB_ISMONITORED == "Y" ? true : false,
                                  LastCheckin = (DateTime)hb.HB_LASTCHECKIN,
                                  Opened = (DateTime)hb.HB_OPENED,
                                  LastOperation = hb.HB_LASTOPERATION

                              }).OrderBy(x => x.Customer)
                                  .ToList();
                }
                else
                {
                    hbList = (from hb in uow.HeartBeats.Find(x => x.CustomerLookup.C_NAME == "CTC")
                                    .Include(x => x.CustomerLookup)
                              select new HeartBeatList
                              {
                                  ApplicationName = hb.HB_NAME,
                                  Customer = hb.CustomerLookup.C_NAME,
                                  IsMonitored = hb.HB_ISMONITORED == "Y" ? true : false,
                                  LastCheckin = (DateTime)hb.HB_LASTCHECKIN,
                                  Opened = (DateTime)hb.HB_OPENED,
                                  LastOperation = hb.HB_LASTOPERATION

                              }).OrderBy(x => x.Customer)
                                  .ToList();
                }

                

                return View(hbList);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}