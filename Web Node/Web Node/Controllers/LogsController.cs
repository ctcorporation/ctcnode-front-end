﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Web_Node.Classes;

namespace Web_Node.Controllers
{
    [System.Web.Mvc.Authorize]
    public class LogsController : Controller
    {

        //Decorate with a HttpPost meaning it will only ever excute on a HTTP Post operation. If it was ever triggered with a Get it will be ignored. 
        [HttpPost]
        public ActionResult PurgeLogs(string appName, string serverName, string dfrom, string dto, string information, string warning, string error)
        {
            // The Parameters are passed from the ajax function
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                bool infoChecked = !string.IsNullOrEmpty(information) ? bool.Parse(information) : false;
                bool warnChecked = !string.IsNullOrEmpty(warning) ? bool.Parse(warning) : false;
                bool errChecked = !string.IsNullOrEmpty(error) ? bool.Parse(error) : false;
                //Create a list of APPLOG records to be deleted
                IQueryable<AppLog> logList = uow.AppLogs.GetAll();
                var dellogList = logList.If(!string.IsNullOrEmpty(appName),
                         q => q.Where(x => x.GL_AppName == appName))
                     .If(!string.IsNullOrEmpty(serverName),
                         q => q.Where(x => x.GL_ServerName == serverName))
                     .If(infoChecked,
                         q => q.Where(x => x.GL_MessageType == "Information"))
                     .If(warnChecked,
                         q => q.Where(x => x.GL_MessageType == "Warning"))
                     .If(errChecked,
                         q => q.Where(x => x.GL_MessageType == "Error"))
                     .ToList();


                //logList = !string.IsNullOrEmpty(appName) ? from x in logList
                //                                           where x.GL_AppName == appName
                //                                           select x : logList;
                //uow.AppLogs.RemoveRange(logList.ToList());
                //logList = !string.IsNullOrEmpty(serverName) ? from x in logList
                //                                              where x.GL_ServerName == serverName
                //                                              select x : logList;
                //uow.AppLogs.RemoveRange(logList.ToList());
                //if (!string.IsNullOrEmpty(information))
                //{
                //    if (bool.Parse(information))
                //    {
                //        logList = from x in logList
                //                  where x.GL_MessageType == "Information"
                //                  select x;
                //        uow.AppLogs.RemoveRange(logList.ToList());
                //    }
                //}
                //if (!string.IsNullOrEmpty(warning))
                //{
                //    if (bool.Parse(warning))
                //    {
                //        logList = from x in logList
                //                  where x.GL_MessageType == "Warning"
                //                  select x;
                //        uow.AppLogs.RemoveRange(logList.ToList());
                //    }
                //}
                //if (!string.IsNullOrEmpty(error))
                //{
                //    if (bool.Parse(error))
                //    {
                //        logList = from x in logList
                //                  where x.GL_MessageType == "Error"
                //                  select x;
                //        uow.AppLogs.RemoveRange(logList.ToList());
                //    }
                //}
                //Now we go and collect the records to be deleted and pass it to the DTO (UnitOfWork) to do the Database work. 
                uow.AppLogs.RemoveRange(dellogList);
                uow.Complete();
            }


            return RedirectToAction("Index");
        }
        // GET: Logs
        public ActionResult Index(DateTime? dfrom, DateTime? dto, string ddlApplication, string ddlServer, string cbInformation, string cbWarning, string cbError)
        {
            // The Index function is the default "READ" function and default ACTION for this controller
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                IList<AppLog> logs;
                //Create a list of distinct Applications
                var apps = uow.AppLogs.GetAll().Select(ap => ap.GL_AppName).Distinct().ToList();
                //Create a list of distinct Servers
                var servers = uow.AppLogs.GetAll().Select(ap => ap.GL_ServerName).Distinct().ToList();
                //Create a new SelectList that will be used to fill the Application Drop down List
                ViewBag.AppList = new SelectList(apps, "GL_AppName");
                // Create a new SelectList that will be used to fill the SERVER drop down list
                ViewBag.ServerList = new SelectList(servers, "GL_ServerName");
                //Set the default Date Ranges
                ViewBag.DFrom = DateTime.Now.AddDays(-7);
                ViewBag.DTo = DateTime.Now;
                //If no Application is passed grab everything in the AppLog table as an IQueryable
                var logList = string.IsNullOrEmpty(ddlApplication) ? uow.AppLogs.GetAll() : uow.AppLogs.Find(x => x.GL_AppName == ddlApplication);
                //Now that we have a logList IQueryable we pass this through the filters to get to the required list. 
                if (!string.IsNullOrEmpty(ddlServer))
                {
                    //Equiv SQL is = Select *  from logList where GL_AppName = "ddlServer Value"
                    logList = from x in logList
                              where x.GL_AppName == ddlServer
                              select x;
                }
                //cb denotes CheckBox. Value is true if checked. 
                if (!string.IsNullOrEmpty(cbInformation))
                {
                    if (bool.Parse(cbInformation))
                    {
                        logList = from x in logList
                                  where x.GL_MessageType == "Information"
                                  select x;
                    }
                }
                if (!string.IsNullOrEmpty(cbWarning))
                {
                    if (bool.Parse(cbWarning))
                    {
                        logList = from x in logList
                                  where x.GL_MessageType == "Warning"
                                  select x;
                    }
                }
                if (!string.IsNullOrEmpty(cbError))
                {
                    if (bool.Parse(cbError))
                    {
                        logList = from x in logList
                                  where x.GL_MessageType == "Error"
                                  select x;
                    }
                }
                

                if(dfrom != null && dto != null)
                {
                    logList = from x in logList
                              where x.GL_Date >= dfrom && x.GL_Date <= dto
                              select x;
                }else if(dfrom != null && dto == null)
                {
                    logList = from x in logList
                              where x.GL_Date >= dfrom
                              select x;
                }
                else if (dfrom == null && dto != null)
                {
                    logList = from x in logList
                              where x.GL_Date <= dto
                              select x;
                }
                else
                {
                    logList = from x in logList
                              where x.GL_Date.Month == DateTime.Now.Month
                    && x.GL_Date.Day == DateTime.Now.Day
                    && x.GL_Date.Year == DateTime.Now.Year
                              select x;
                }


                
                //The process of sending the IQueryable to a List is the actual Read from the Database where data is transferred back to the View. 
                return View(logList.ToList());
            }
        }



    }
}