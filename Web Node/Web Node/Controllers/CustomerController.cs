﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Web_Node.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Web;

namespace Web_Node.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            CustomerViewModel customer = new CustomerViewModel();
            ApplicationUserManager UserManager;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                if (User.IsInRole("Administrator"))
                {
                    customer.Customers = uow.Customers.GetAll().OrderBy(x => x.C_NAME).ToList();
                }
                else
                {
                    UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
                    customer.Customers = uow.Customers.Find(x => x.C_CODE == user.CompanyCode).OrderBy(x => x.C_NAME).ToList();

                }
            }
            return View(customer);
        }

        [HttpGet]
        public ActionResult Details(Guid? id)
        {
            var cust = new CustomerViewModel();
            if (id == null)
            {
                cust.EditMode = "New";
                ViewBag.Title = "Add New Customer";
                cust.Customer = new Customer();
                return View(cust);
            }
            ViewBag.Title = "Edit Customer Details";
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                cust.EditMode = "Edit";
                cust.Customer = uow.Customers.Get((Guid)id);
                return View(cust);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(CustomerViewModel cust, string C_IS_ACTIVE, string C_ON_HOLD)
        {
            //if (cust.EditMode == "New")
            //{
            //    Add(cust);
            //};
            //var id = cust.Customer.C_ID;
            //if (cust.Customer.C_ID == Guid.Empty)
            //{
            //    cust.EditMode = "New";
            //    ViewBag.Title = "Add New Customer";
            //    cust.Customer = new Customer();
            //    return View(cust);
            //}
            //ViewBag.Title = "Edit Customer Details";
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                var customer = uow.Customers.Get((Guid)cust.Customer.C_ID);

                customer.C_NAME = cust.Customer.C_NAME;
                customer.C_SHORTNAME = cust.Customer.C_SHORTNAME;
                customer.C_CODE = cust.Customer.C_CODE;
                customer.C_PATH = cust.Customer.C_PATH;
                if (C_IS_ACTIVE == "true")
                {
                    customer.C_IS_ACTIVE = "Y";
                }
                else
                {
                    customer.C_IS_ACTIVE = "N";
                }

                if (C_ON_HOLD == "true")
                {
                    customer.C_ON_HOLD = "Y";
                }
                else
                {
                    customer.C_ON_HOLD = "N";
                }

                uow.Complete();
            }

            //return View(cust);
            return RedirectToAction("Index");
        }


        public ActionResult Add(CustomerViewModel cust)
        {
            var newcust = new Customer
            {
                C_CODE = cust.Customer.C_CODE,
                C_NAME = cust.Customer.C_NAME,
                C_SHORTNAME = cust.Customer.C_SHORTNAME,
                C_IS_ACTIVE = cust.Customer.C_IS_ACTIVE,
                C_PATH = cust.Customer.C_PATH,
                C_INVOICED = string.IsNullOrEmpty(cust.Customer.C_INVOICED) ? "N" : cust.Customer.C_INVOICED,
                C_ON_HOLD = string.IsNullOrEmpty(cust.Customer.C_ON_HOLD) ? "N" : cust.Customer.C_ON_HOLD,
                C_FTP_CLIENT = string.IsNullOrEmpty(cust.Customer.C_FTP_CLIENT) ? "N" : cust.Customer.C_FTP_CLIENT,
                C_TRIAL = string.IsNullOrEmpty(cust.Customer.C_TRIAL) ? "N" : cust.Customer.C_TRIAL,
                C_SATELLITE = string.IsNullOrEmpty(cust.Customer.C_SATELLITE) ? "N" : cust.Customer.C_SATELLITE
            };
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                var existingCustomer = uow.Customers.Find(x => x.C_NAME == cust.Customer.C_NAME || x.C_CODE == cust.Customer.C_CODE || x.C_SHORTNAME == cust.Customer.C_SHORTNAME).FirstOrDefault();
                if (existingCustomer != null)
                {
                    return RedirectToAction("Details");

                }
                uow.Customers.Add(newcust);
                uow.Complete();

            }
            return RedirectToAction("Details", newcust.C_ID.ToString());
        }


        [HttpGet]
        public ActionResult ProfileListing(Guid custId)
        {
            List<Profile> profiles = new List<Profile>();
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                profiles = uow.Profiles.Find(x => x.P_C == custId)
                    .OrderBy(x => x.P_SENDERID)
                    .ThenBy(x => x.P_RECIPIENTID)
                    .ToList();
            }

            return PartialView(profiles);
        }


    }
}