﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using Web_Node.Models;
using Microsoft.AspNet.Identity;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;

namespace Web_Node.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Profile
        [HttpGet]
        public ActionResult Index(Guid? id)
        {

            ProfileViewModel profile = new ProfileViewModel();
            ApplicationUserManager UserManager;

            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                if(id != null)
                {
                    profile.Profiles = uow.CustProfiles.Find(x => x.P_C == (Guid)id).OrderBy(x => x.C_NAME).ToList();
                }
                else
                {
                    if (User.IsInRole("Administrator"))
                    {
                        profile.Profiles = uow.CustProfiles.GetAll().OrderBy(x => x.C_NAME).ToList();
                    }
                    else
                    {
                        UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                        ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
                        profile.Profiles = uow.CustProfiles.Find(x => x.C_CODE == user.CompanyCode).OrderBy(x => x.C_NAME).ToList();

                    }
                    
                }
                
            }
            return View(profile);
        }

        [HttpGet]
        public ActionResult Tasklist(Guid? id, DateTime? from = null, DateTime? to = null)
        {
            ViewBag.from = "";
            ViewBag.to = "";
            var task = new TaskListViewModel();
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                if (from != null && to != null)
                {
                    TimeSpan tFrom = new TimeSpan(0, 0, 0);
                    DateTime dFrom = Convert.ToDateTime(from) + tFrom;
                    TimeSpan tTo = new TimeSpan(23, 59, 59);
                    DateTime dTo = Convert.ToDateTime(to) + tTo;
                    task.Tasks = uow.TaskLists.Find(x => x.TL_P == (Guid)id && x.TL_ReceiveDate >= dFrom && x.TL_ReceiveDate <= dTo).OrderByDescending(x => x.TL_ReceiveDate).ToList();
                }
                else
                {
                    task.Tasks = uow.TaskLists.Find(x => x.TL_P == (Guid)id 
                    && x.TL_ReceiveDate.Value.Month == DateTime.Now.Month
                    && x.TL_ReceiveDate.Value.Day == DateTime.Now.Day
                    && x.TL_ReceiveDate.Value.Year == DateTime.Now.Year
                    ).OrderByDescending(x => x.TL_ReceiveDate).ToList();
                }
                task.Profile = uow.Profiles.Get((Guid)id);
            }
            if(from != null)
            {
                ViewBag.from = from.Value.ToString("yyyy-MM-dd");
            }

            if (to != null)
            {
                ViewBag.to = to.Value.ToString("yyyy-MM-dd");
            }

            return View(task);
        }

        // GET: Profile/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Profile/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Profile/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Profile/Edit/5
        [HttpGet]
        public ActionResult Edit(Guid? id)
        {
            ProfileViewModel profile = new ProfileViewModel();
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                profile.Profile = new Profile();
                profile.CustProfile = uow.CustProfiles.Find(x => x.P_ID == (Guid)id).FirstOrDefault();
                profile.Profile = uow.Profiles.Find(x => x.P_ID == (Guid)id).FirstOrDefault();
            }

            ViewBag.P_MSGTYPE = new SelectList(new[] { "Original", "Response", "Forward", "eAdapter", "Conversion", "PDF2XML" });
            ViewBag.P_BILLTO = new List<SelectListItem>() { new SelectListItem { Value = profile.CustProfile.C_CODE, Text = profile.CustProfile.C_CODE + "(Customer)" }, new SelectListItem { Value = profile.Profile.P_RECIPIENTID, Text = profile.Profile.P_RECIPIENTID+"(Vendor)" } };
            ViewBag.P_DIRECTION = new List<SelectListItem>() { new SelectListItem { Value = "R", Text = "Receive" }, new SelectListItem { Value = "S", Text = "Send" } };
            return View(profile);
        }

        // POST: Profile/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Profile model)
        {

            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext()))
            {
                var prof = uow.Profiles.Get((Guid)model.P_ID);

                prof.P_DESCRIPTION = model.P_DESCRIPTION;
                prof.P_SENDERID = model.P_SENDERID;
                prof.P_RECIPIENTID = model.P_RECIPIENTID;
                prof.P_REASONCODE = model.P_REASONCODE;
                prof.P_EVENTCODE = model.P_EVENTCODE;
                prof.P_SUBJECT = model.P_SUBJECT;
                prof.P_MSGTYPE = model.P_MSGTYPE;
                prof.P_DIRECTION = model.P_DIRECTION;
                prof.P_BILLTO = model.P_BILLTO;
                if (model.P_ACTIVE == "true")
                {
                    prof.P_ACTIVE = "Y";
                }
                else
                {
                    prof.P_ACTIVE = "N";
                }
                
                prof.P_SERVER = model.P_SERVER;
                prof.P_PORT = model.P_PORT;
                prof.P_PATH = model.P_PATH;
                prof.P_USERNAME = model.P_USERNAME;
                if (model.P_SSL == "true")
                {
                    prof.P_SSL = "Y";
                }
                else
                {
                    prof.P_SSL = "N";
                }
                prof.P_LIBNAME = model.P_LIBNAME;
                prof.P_METHOD = model.P_METHOD;
                prof.P_PARAMLIST = model.P_PARAMLIST;

                uow.Complete();

            }

                return RedirectToAction("Index");
           
        }

        // GET: Profile/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Profile/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
